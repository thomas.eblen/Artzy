<?php
	/* All php pages/actions are called through routes. When adding new page/action, must add here. */
	require_once($config["basic"]["rootPath"] . '/Artzy/src/models/sqlmodels/SQLInterface.php');
	require_once($config["basic"]["rootPath"] . "/Artzy/src/models/s3models/S3Interface.php");




	function call($controllerName, $action, $conn, $config){

		$s3 = require $config["basic"]["rootPath"] . "/Artzy/config/S3Connect.php";

		$fileUploader = new S3Interface($config['s3']['bucket'], $s3);

		$sqlInterface = new SQLInterface($conn);


		require_once($config["basic"]["rootPath"] . "/Artzy/src/controllers/{$controllerName}_controller.php");

		switch($controllerName){
			/*   php viewable sites    */
			case 'test':
				$controller = new TestController();
				break;
			case 'login':
				$controller = new LoginController();
				break;
			case 'createAcc':
				$controller = new CreateAccountController();
				break;
			case 'uploadMedia':
				$controller = new UploadMediaController();
				break;
			case 'messageViewer':
				$controller = new MessageViewerController($sqlInterface);
				break;
			case 'muralReview':
				$controller = new MuralReviewController();
				break;
			case 'groupViewer':
				$controller = new GroupViewerController($sqlInterface);
				break;
			case 'profileViewer':
				$controller = new ProfileViewerController($sqlInterface);
				break;
			case 'mediaViewer':
				$controller = new MediaViewerController($sqlInterface);
				break;
			case 'logout':
				$controller = new LogoutController();
				break;
			case 'profileEditor':
				$controller = new ProfileEditorController($sqlInterface);
				break;
			case 'userProfile':
				$controller = new UserProfileController();
				break;
				
			/*      php actions      */
			
			case 'loginAction':
				$controller = new LoginActionController();
				break;
			case 'createAccAction':
				$controller = new CreateAccountActionController();
				break;
			case 'addLikeAction':
				$controller = new AddLikeActionController();
				break;
			case 'uploadImageAction':
				$controller = new UploadImageActionController();
				break;
			case 'uploadAudioAction':
				$controller = new UploadAudioActionController();
				break;
			case 'uploadTextAction':
				$controller = new UploadTextActionController();
				break;
			case 'deleteMediaAction':
				$controller = new DeleteMediaActionController();
				break;
			case 'addNormalFormatMediaAction':
				$controller = new AddNormalFormatMediaActionController();
				break;
			case 'sendMessageAction':
				$controller = new SendMessageActionController();
				break;
			case 'loadSearchGroupsAction':
				$controller = new LoadSearchGroupsActionController();
				break;
			case 'addCommentAction':
				$controller = new AddCommentActionController();
				break;
			case 'loadCommentsAction':
				$controller = new LoadCommentsActionController();
				break;
			case 'verifyAccountAction':
				$controller = new VerifyAccountActionController();
				break;
				
				


		}

		$controller->{$action}();


	}

	$controllers = array(
						/* viewable sites */
						'test' => ['home', 'error'],
						'login' => ['home', 'error'],
						'createAcc' => ['home', 'error'],
						'uploadMedia' => ['home', 'error'],
						'messageViewer' => ['home', 'error'],
						'muralReview' => ['home', 'error'],
						'groupViewer' => ['home', 'error'],
						'profileViewer' => ['home', 'error'],
						'mediaViewer' => ['home', 'error'],
						'logout' => ['home', 'error'],
						'profileEditor' => ['home', 'error'],
						'userProfile' => ['home', 'error'],
						/* actions */
						'loginAction' => ['home', 'error'],
						'createAccAction' => ['action', 'error'],
						'addLikeAction' => ['action', 'error'],
						'addCommentAction' => ['action', 'error'],
						'deleteMediaAction' => ['action', 'error'],
						'sendMessageAction' => ['action', 'error'],
						'uploadTextAction' => ['action', 'error'],
						'uploadImageAction' => ['action', 'error'],
						'uploadAudioAction' => ['action', 'error'],
						'addNormalFormatMediaAction' => ['action'],
						'sendMessageActionController' => ['action'],
						'loadSearchGroupsAction' => ['action'],
						'addCommentAction' => ['action'],
						'loadCommentsAction' => ['action'],
						'verifyAccountAction' =>['action']
						
					);


	if (array_key_exists($controller, $controllers)) {
		if (in_array($action, $controllers[$controller])){
			call($controller, $action, $conn, $config);
		}else{
			call('test', 'error', $conn, $config);
		}
	}else{
		call('test', 'error', $conn, $config);
	}

?>
