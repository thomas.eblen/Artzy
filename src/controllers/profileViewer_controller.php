<?php
  class ProfileViewerController{
    protected $config;
    function __construct($sqlInterface){
      $this->sqlInterface = $sqlInterface;

      $this->config = require('../config/config.php');
    }
    function home(){
      $config = $this->config;
      $username = $_GET["username"];
      if (!isset($username))
        return 0;

      $user = $this->sqlInterface->getUser($username);

      $mediaData = $this->sqlInterface->getMediaIdsFromUser($user["id"]);



      $mediaIds = array();
      for ($i = 0; $i < sizeof($mediaData); $i++){
				array_push($mediaIds, $mediaData[$i]["id"]);
			}

      $media = $this->sqlInterface->getArrayOfMedia($mediaIds);
      $media = $this->sqlInterface->setMediaSpecificData($media);



      require_once($config['basic']['rootPath'] . "/Artzy/src/views/ProfileViewer/ProfileViewer.php");
    }

    function error(){

    }
  }

 ?>
