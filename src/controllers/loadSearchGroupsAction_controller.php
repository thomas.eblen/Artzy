<?php
	class LoadSearchGroupsActionController{
		protected $config;
	
		function __construct(){
			
			$this->config = require("../config/config.php");
		}
		
		function action(){
			$config = $this->config;
			//echo "load search action controller";
			require_once("LiveSearch/LiveSearch.php");
		}
	}

?>