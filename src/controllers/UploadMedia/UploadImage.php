<?php
 
  require_once ($config['basic']['rootPath']. "/Artzy/src/connections/connection.php");
  require_once ($config['basic']['rootPath'] . "/Artzy/src/models/s3models/S3Interface.php");
  require_once ($config['basic']['rootPath'] . "/Artzy/src/models/sqlmodels/SQLInterface.php");


  
  $s3 = require $config['basic']['rootPath'] . "/Artzy/config/S3Connect.php";

  $conn = Db::getInstance($config);

  $sql = new SQLInterface($conn);
  $fileUploader = new S3Interface($config['s3']['bucket'], $s3);

  $imagetemp=$_FILES["uploadedImage"]["tmp_name"];
  list($imageWidth, $imageHeight) = getimagesize($imagetemp);

  $mediaId = $sql->uploadMedia($_SESSION["currentId"], 1);
  $audioId = $sql->uploadImage($_POST["imageName"], $imageWidth, $imageHeight, $mediaId);

  $uploadStatus = $fileUploader->uploadS3File($_FILES, $config['basic']['tempFilesPath'], "UserData/ImagePost/", "uploadedImage", $mediaId . ".jpg");

  if ( $uploadStatus == 0){
	   echo "audiofile moved";
  }else{
	echo $_FILES["uploadedImage"]["error"];
	die("audio file move failed: " . $uploadStatus);
  }

  $groups = explode(" ", " " . $_POST["groups"]);
  $sql->uploadGroupLinks($groups, $mediaId);

  echo "upload image";

  header('Location: index.php?controller=uploadMedia&action=home');
 ?>
