<?php
 
  require_once ($config['basic']['rootPath'] . "/Artzy/src/connections/connection.php");
  require_once ($config['basic']['rootPath'] . "/Artzy/src/models/s3models/S3Interface.php");
  require_once ($config['basic']['rootPath'] . "/Artzy/src/models/sqlmodels/SQLInterface.php");

  $s3 = require($config['basic']['rootPath'] . "/Artzy/config/S3Connect.php");

  $conn = Db::getInstance($config);

  $sql = new SQLInterface($conn);
  $fileUploader = new S3Interface($config['s3']['bucket'], $s3);


  //uploads audio data. Must change 173 to session user when setup
  $mediaId = $sql->uploadMedia($_SESSION["currentId"], 4);
  $audioId = $sql->uploadAudio($_POST["audioName"], $mediaId);

  //upload files to aws s3
  
  $uploadStatus = $fileUploader->uploadS3File($_FILES, $config['basic']['tempFilesPath'], "UserData/AudioPost/Audio/", "uploadedAudio", $mediaId . ".mp3");

  if ( $uploadStatus == 0){
	   echo "audiofile moved";
  }else{
	echo $_FILES["uploadedAudio"]["error"];
	 die("audio file move failed: " . $uploadStatus);
  }

  $groups = explode(" ", " " . $_POST["groups"]);
  $sql->uploadGroupLinks($groups, $mediaId);

  echo "upload Audio";

  header('Location: /Artzy/index.php?controller=uploadMedia&action=home');
  
 ?>
