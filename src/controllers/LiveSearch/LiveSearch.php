<?php
  require_once($config["basic"]["rootPath"] . '/Artzy/src/models/sqlmodels/SQLInterface.php');
  require_once($config["basic"]["rootPath"] . "/Artzy/src/connections/connection.php");
  require_once($config["basic"]["rootPath"] . "/Artzy/src/controllers/EmailLibrary/EmailInterface.php");
  require_once($config["basic"]["rootPath"] . "/Artzy/src/views/DisplayHelpers/DisplayMedia.php");

  $conn = Db::getInstance($config);

  $sql = new SQLInterface($conn);

  $groups = $sql->getGroupsFromSearch($_GET["searchValue"]);

  foreach ($groups as $group){
    echo "<a href = index.php?controller=groupViewer&action=home&group={$group}>{$group}<a/><br/>";
  }
 ?>
