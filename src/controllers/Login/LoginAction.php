<?php
	//session_start();

	require_once($config["basic"]["rootPath"] . '/Artzy/src/controllerHelpers/ErrorReports.php');
	require_once($config["basic"]["rootPath"] . "/Artzy/src/connections/connection.php");
	require_once($config["basic"]["rootPath"] . '/Artzy/src/models/sqlmodels/SQLInterface.php');

	$config = require $config["basic"]["rootPath"] . "/Artzy/config/config.php";
	$conn = Db::getInstance($config);

	$sqlInterface = new SQLInterface($conn);



	$user=$_POST["username"];
	$pass=$_POST["password"];

	$result = $sqlInterface->getLoginUser($user, $pass);

	if ($result != NULL){
		echo 'not null';

		if ($result["isActivated"] == 1){

			$_SESSION["currentUser"] = $user;
			$_SESSION["currentId"] = $result["id"];
			$_SESSION["currentPass"] = $pass;

			setcookie("ARTZY_USERNAME", $user, time() + (86400 * 30), "/");
			setcookie("ARTZY_PASSWORD", $pass, time() + (86400 * 30), "/");

			//header( 'Location: ../ProfilePage/profilePage.php' );
			header( 'Location: index.php?controller=groupViewer&action=home' );
		}else{
			$_SESSION["m_Login"]="Unverified Account. Check your email to verify(make sure to check your SPAM folder).";
			header('Location: index.php?controller=login&action=home');
		}

	}else{
		if($_GET["cookie"] == 1){
			$_SESSION["cookie"] = "1" ;
			header('Location: index.php?controller=login&action=home');

		}else{
			$_SESSION["m_Login"] = "password or username incorrect" ;
			header('Location: index.php?controller=login&action=home');
			echo "no user found";
		}


	}

?>
