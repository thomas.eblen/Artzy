<?php
  //displays one media; currently doesn't display media optons
  class MediaViewerController{
	protected $config;
	
    function __construct($sqlInterface){
      $this->sqlInterface = $sqlInterface;
	  $this->config = require("../config/config.php");
    }
    function home(){
		$config = $this->config;
      echo "media viewer";

      $mediaId = $_GET["mediaId"];

      $media = $this->sqlInterface->getArrayOfMedia(array($mediaId));
      $media = $this->sqlInterface->setMediaSpecificData($media);

      //var_dump($media);

      require_once $config['basic']['rootPath'] . "/Artzy/src/views/MediaViewer/MediaViewer.php";

    }
    function error(){

    }
  }
 ?>
