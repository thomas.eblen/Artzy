<?php
	class UserProfileController{
		protected $config;
		
		function __construct(){
			$this->config = require("../config/config.php");
		}
		
		function home(){
			$config = $this->config;
			
			require($config['basic']['rootPath'] . "/Artzy/src/views/Header/Header.php");
			require($config['basic']['rootPath'] . "/Artzy/src/views/UserProfile/UserProfile.php");
		}
		
		function error(){
			
		}
		
		
	}


?>