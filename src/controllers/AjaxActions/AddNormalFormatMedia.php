<?php
  require_once($config['basic']['rootPath'] . "/Artzy/src/views/DisplayHelpers/DisplayMedia.php");

  $config = require_once($config['basic']['rootPath'] . "/Artzy/config/config.php");
  $displayMedia = new DisplayMedia();
  $media = json_decode($_POST["jsonMedia"], true);

  echo $displayMedia->loadMedia($_POST["currentId"], $media, $_POST["nMedia"], $_POST["nMediaToLoad"], $_POST["mediaOptionsType"], $config);
 
 ?>