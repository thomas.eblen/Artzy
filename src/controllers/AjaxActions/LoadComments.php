<?php
  require_once($config["basic"]["rootPath"] . '/Artzy/src/models/sqlmodels/SQLInterface.php');
  require_once($config["basic"]["rootPath"] . "/Artzy/src/connections/connection.php");
  require_once($config["basic"]["rootPath"] . "/Artzy/src/controllers/EmailLibrary/EmailInterface.php");
  require_once($config["basic"]["rootPath"] . "/Artzy/src/views/DisplayHelpers/DisplayMedia.php");

  $config = require $config["basic"]["rootPath"] . "/Artzy/config/config.php";
  $conn = Db::getInstance($config);

  $sql = new SQLInterface($conn);
  $display = new DisplayMedia();

  $comments = json_decode($_POST["jsonComments"], true);


  echo $display->loadComments($comments, $_POST["nComments"], $_POST["nCommentsToLoad"]);
  //echo "load comments place holder";
 ?>
