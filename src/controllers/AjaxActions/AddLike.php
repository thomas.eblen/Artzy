<?php
  require_once($config['basic']['rootPath'] . '/Artzy/src/models/sqlmodels/SQLInterface.php');
  require_once($config['basic']['rootPath'] . "/Artzy/src/connections/connection.php");

  $config = require $config['basic']['rootPath'] . "/Artzy/config/config.php";
  $conn = Db::getInstance($config);

  $sql = new SQLInterface($conn);
  
  $likesFromUser = $sql->getLikesFromUser($_GET['userId'], $_GET['mediaId']);
  
  if($likesFromUser != null) {
	  return 2;
  }
  
  $returnValue = $sql->insertLike($_GET["mediaId"], $_GET["userId"]);
  return $returnValue;
  
 ?>
