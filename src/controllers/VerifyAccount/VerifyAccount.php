<?php
	require_once($config["basic"]["rootPath"] . '/Artzy/src/models/sqlmodels/SQLInterface.php');
	require_once($config["basic"]["rootPath"] . "/Artzy/src/connections/connection.php");
	require_once($config["basic"]["rootPath"] . "/Artzy/src/controllers/EmailLibrary/EmailInterface.php");
	require_once($config["basic"]["rootPath"] . "/Artzy/src/views/DisplayHelpers/DisplayMedia.php");
  
	$conn = Db::getInstance($config);
	$sql = new SQLInterface($conn);

	if (isset($_GET["code"]) && isset($_GET["userId"]) ) {
		$verificationCode = $_GET["code"];
		$id = $_GET["userId"];

		$user = $sql->getUsername($id);

		if( $user["verificationCode"] == $verificationCode ){

			$sql->updateUserActivation($user["id"], 1);
			echo "account verified";
			
			$_SESSION["m_Login"] = "Account activated. Welcome!";
			header('Location: index.php?controller=login&action=home');
		}else{
			echo "codes don't match";
		}
	} else {
		die("ERROR: ID and/or code not set in verifier");
	}




?>
