<?php
  class ProfileEditorController{
    protected $config;
    function __construct($sqlInterface){
      $this->sqlInterface = $sqlInterface;
      $this->config = require('../config/config.php');
    }
    function home(){
      $config = $this->config;
      $username = $_SESSION["currentUser"];
      $id = $_SESSION["currentId"];

      $mediaData = $this->sqlInterface->getMediaIdsFromUser($id);



      $mediaIds = array();
      for ($i = 0; $i < sizeof($mediaData); $i++){
        array_push($mediaIds, $mediaData[$i]["id"]);
      }

      $media = $this->sqlInterface->getArrayOfMedia($mediaIds);
      $media = $this->sqlInterface->setMediaSpecificData($media);



      require_once($config['basic']['rootPath'] . "/Artzy/src/views/ProfileEditor/ProfileEditor.php");
    }
    function error(){

    }
  }
 ?>
