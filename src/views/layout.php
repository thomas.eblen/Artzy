<?php 
	session_start(); 
	
	$routesPath = $config["basic"]["rootPath"] . "/Artzy/src/routes/routes.php";
	
	/* If noLayout not set include basic body and head layout. noLayout set when using ajax actions that require return result */
	if (!isset($_GET["noLayout"]) ){
		echo"
		<!DOCTYPE html>
		<html>
			<head>
				<title>test title</title>
				<meta charset='utf-8'>
			</head>
			<body>
		";
		require_once($routesPath);
		
		echo "</html></body>";
	}else {
		require_once($routesPath);
	}
	
	


?>



