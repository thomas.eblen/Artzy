<?php

class LayoutHelper{
	
	function requireInLayout($pathToRequire, $pageTitle){
		echo"
		<!DOCTYPE html>
		<html>
			<head>
				<title>{$pageTitle}</title>
				<meta charset='utf-8'>
			</head>
			<body>
		";
		
		require_once($pathToRequire);
		
		echo "</html></body>";
		
	}
	
}


?>