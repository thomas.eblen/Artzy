<?php
	/*
		includes connection library and config file, and sets controller and action
	variables, then includes basic layout which uses action and controller to
	determine file to include using routes file.
	*/

	$config = require "../config/config.php";

	require_once($config["basic"]["rootPath"] . "/Artzy/src/connections/connection.php");

	$conn = Db::getInstance($config);
	if ( isset($_GET['controller']) && isset($_GET['action']) ){
		$controller = $_GET['controller'];
		$action = $_GET['action'];
	}else{
		header('Location: index.php?controller=login&action=home');
		$controller = 'test';
		$action = 'error';
	}
	require_once($config["basic"]["rootPath"] . "/Artzy/src/views/layout.php");


?>
